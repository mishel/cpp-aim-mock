<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>হিসাবরক্ষণ</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Project Cash Return</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Project Cash Return
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-body form">
										<form action="" class="row">
											<div class="form-group col-md-3">
												<label>Source</label>
												<input type="text" class="form-control" value="Station" readonly>
											</div>
											<div class="form-group col-md-3">
												<label>Project name</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>Project 1</option>
													<option>Project 2</option>
													<option>Project 3</option>
												</select>
											</div>
											<div id="station" class="src">
												<div class="form-group col-md-3">
													<label>জোন</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>জোন ১</option>
														<option>জোন ২</option>
														<option>জোন ৩</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>স্টেশন</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>স্টেশন ১</option>
														<option>স্টেশন ২</option>
														<option>স্টেশন ৩</option>
													</select>
												</div>
											</div>
											<div id="person" class="src hidden">
											<div class="form-group col-md-3">
													<label>জোন</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>জোন ১</option>
														<option>জোন ২</option>
														<option>জোন ৩</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>স্টেশন</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>স্টেশন ১</option>
														<option>স্টেশন ২</option>
														<option>স্টেশন ৩</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Name</label>
													<input type="text" class="form-control">
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>অর্থ ফেরতের পদ্ধতি</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>চেক</option>
													<option>ব্যাংক ড্রাফট</option>
													<option>টি. টি</option>
													<option>নগদ</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>রেফারেন্স নং</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>ব্যাংকের নাম</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>টাকার পরিমাণ</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>তারিখ</label>
												<input class="form-control date-picker" size="16" type="text">
											</div>
											<div class="form-group col-md-6">
												<label>মন্তব্য</label>
												<textarea class="form-control" rows="3"></textarea>
											</div>
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="prj_cash_return.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>

		<script>
			(function() {
				document.querySelector("#src").addEventListener("change", function(e) {
					var val = e.target.value;
					var blocks = document.querySelectorAll(".src");
					if (val === "Station") {
						blocks.forEach((el) => {
							el.classList.add("hidden");
						});
						document.querySelector("#station").classList.remove("hidden");
					}
					else if (val === "Person") {
						blocks.forEach((el) => {
							el.classList.add("hidden");
						});
						document.querySelector("#person").classList.remove("hidden");
					}
				});
			})();
		</script>
	</body>
</html>


<p>src station will also have a person name field</p>
