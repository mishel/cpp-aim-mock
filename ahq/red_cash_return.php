<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>হিসাবরক্ষণ</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Cash Return</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Cash Return
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-title text-right">
										<a href="red_cash_return-add.php" class="btn sbold green"> নতুন যুক্ত করুন
											<i class="fa fa-plus"></i>
										</a>
									</div>
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-hover order-column" id="sample_1">
											<thead>
												<tr>
													<th>Source</th>
													<th>Head</th>
													<th>Subhead</th>
													<th>Amount</th>
													<th>Date</th>
													<th>Remarks</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<tr class="odd gradeX">
													<td>Govt.</td>
													<td>XYZ</td>
													<td>ABC</td>
													<td>10000000.00</td>
													<td>18/01/2019</td>
													<td>Asdfghj</td>
													<td>
														<button class="btn btn-xs green" type="button">&nbsp;&nbsp; Edit &nbsp;&nbsp;</button>
														<button class="btn btn-xs red" type="button">Delete</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<!-- END PORTLET-->
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>
	</body>
</html>
