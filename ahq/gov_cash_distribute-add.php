<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>হিসাবরক্ষণ</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>অর্থ বিতরণ</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							অর্থ বিতরণ
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-body form">
										<form action="" class="row">
											<div class="form-group col-md-3">
												<label>কাকে বিতরণ করবেন?</label>
												<select id="select_type" class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>স্টেশন</option>
													<option>ব্যক্তি/প্রতিষ্ঠান</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>তারিখ</label>
												<input class="form-control date-picker" size="16" type="text">
											</div>


											<div class="clearfix"></div>


											<div id="for_person" class="hidden">
												<div class="form-group col-md-3">
													<label>ব্যক্তির/প্রতিষ্ঠানের নাম</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>ব্যক্তি/প্রতিষ্ঠান ১</option>
														<option>ব্যক্তি/প্রতিষ্ঠান ২</option>
														<option>ব্যক্তি/প্রতিষ্ঠান ৩</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>পদবী</label>
													<input type="text" class="form-control" disabled>
												</div>
												<div class="form-group col-md-3">
													<label>প্রতিষ্ঠান</label>
													<input type="text" class="form-control" disabled>
												</div>
												<div class="form-group col-md-3">
													<label>ব্যক্তির/প্রতিষ্ঠানের মোবাইল নং</label>
													<input type="text" class="form-control" disabled>
												</div>
												<div class="form-group col-md-3">
													<label>হিসাব</label>
													<input type="text" class="form-control" disabled value="০.০০">
												</div>
												<div class="form-group col-md-3">
													<label>কোড-হেড</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>১২৩৪</option>
														<option>১২৩৫</option>
														<option>১২৩৬</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>কোড-সাবহেড</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>৪৫৬৭</option>
														<option>৪৫৬৮</option>
														<option>৪৫৬৯</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>টাকার পরিমাণ</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>টাকার পরিমাণ (কথায়)</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group col-md-3">
													<label>অর্থ বিতরণের পদ্ধতি</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>চেক</option>
														<option>ব্যাংক ট্রান্সফার</option>
														<option>নগদ</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>চেক/রেফারেন্স নং</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group col-md-3">
													<label>ব্যক্তির/প্রতিষ্ঠানের ব্যাংকের নাম</label>
													<input type="text" class="form-control">
												</div>

												<div class="form-group col-md-3">
													<label>শাখার নাম</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group col-md-3">
													<label>ব্যাংক হিসাবের নাম</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group col-md-3">
													<label>ব্যাংক হিসাবের নং</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>মন্তব্য</label>
													<textarea class="form-control" rows="3"></textarea>
												</div>
											</div>








											<div id="for_station" class="hidden">
												<div class="form-group col-md-3">
													<label>জোন</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>জোন ১</option>
														<option>জোন ২</option>
														<option>জোন ৩</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>স্টেশন</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>স্টেশন ১</option>
														<option>স্টেশন ২</option>
														<option>স্টেশন ৩</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>অর্থ বিতরণের পদ্ধতি</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>চেক</option>
														<option>ব্যাংক ট্রান্সফার</option>
														<option>নগদ</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>চেক/রেফারেন্স নং</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group col-md-3">
													<label>স্টেশনের ব্যাংকের নাম</label>
													<input type="text" class="form-control" disabled value="Bank Asia">
												</div>

												<div class="form-group col-md-3">
													<label>স্টেশনের ব্যাংকের শাখার নাম</label>
													<input type="text" class="form-control" disabled value="Khulna">
												</div>
												<div class="form-group col-md-3">
													<label>স্টেশনের ব্যাংক হিসাবের নাম</label>
													<input type="text" class="form-control" disabled value="Md. Rahim">
												</div>
												<div class="form-group col-md-3">
													<label>স্টেশনের ব্যাংক হিসাবের নং</label>
													<input type="text" class="form-control" disabled value="123456789">
												</div>
												<div class="col-md-12">
													<div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th></th>
																	<th>কোড-হেড</th>
																	<th>কোড-সাবহেড</th>
																	<th>পরিমাণ</th>
																	<th>কথায়</th>
																	<th>স্মারক নং</th>
																	<th>স্মারকের কপি</th>
																	<th>মন্তব্য</th>
																</tr>
															</thead>
															<tbody id="tbody">
																<tr>
																	<td>1</td>
																	<td>
																		<select class="form-control">
																			<option disabled selected>নির্বাচন করুন</option>
																			<option>১২৩৪</option>
																			<option>১২৩৫</option>
																			<option>১২৩৬</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control">
																			<option disabled selected>নির্বাচন করুন</option>
																			<option>৪৫৬৭</option>
																			<option>৪৫৬৮</option>
																			<option>৪৫৬৯</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" class="form-control">
																	</td>
																	<td>
																		<input type="text" class="form-control">
																	</td>
																	<td>
																		<input type="text" class="form-control">
																	</td>
																	<td>
																		<input type="file">
																	</td>
																	<td>
																		<input type="text" class="form-control">
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
													<button type="button" id="add_row" class="btn btn-primary">আরও যুক্ত করুন</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="gov_cash_distribute.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>

		<script>
			(function () {
				document.querySelector("#main").classList.add("active");
				document.querySelector("#gov_acct").classList.add("active");
				document.querySelectorAll("#gov_acct ul li")[3].classList.add("active");
			})();
		</script>

		<script>
			(function() {
				document.addEventListener("click", function(e) {
					if (e.target.id === "add_row") {
						var tbody = document.getElementById("tbody");
						var sl = tbody.querySelectorAll("tr").length;

						var row = `<tr><td>${sl+1}</td><td><select class="form-control"><option disabled selected>নির্বাচন করুন</option><option>১২৩৪</option><option>১২৩৫</option><option>১২৩৬</option></select></td><td><select class="form-control"><option disabled selected>নির্বাচন করুন</option><option>৪৫৬৭</option><option>৪৫৬৮</option><option>৪৫৬৯</option></select></td><td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td><td><input type="file"></td><td><input type="text" class="form-control"></td></tr>`;

						tbody.insertAdjacentHTML("beforeend", row);
					}
				});

				document.addEventListener("change", function(e) {
					if(e.target.id === "select_type") {
						if(e.target.selectedIndex === 1) {
							document.getElementById("for_station").classList.remove("hidden");
							document.getElementById("for_person").classList.add("hidden");
						}
						else {
							document.getElementById("for_person").classList.remove("hidden");
							document.getElementById("for_station").classList.add("hidden");
						}
					}
				})
			})();
		</script>
	</body>
</html>
