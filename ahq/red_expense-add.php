<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>হিসাবরক্ষণ</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Expense</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Add Expense (Filled by Zone/Upazila)
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<!-- <div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												Portlet Captions
											</span>
										</div>
									</div> -->
									<div class="portlet-body form">
										<form action="" class="row">
											<div class="form-group col-md-3">
												<label>ভাউচার নং</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Code No.</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>কোড-হেড</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>১২৩৪</option>
													<option>১২৩৫</option>
													<option>১২৩৬</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>কোড-সাবহেড</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>৪৫৬৭</option>
													<option>৪৫৬৮</option>
													<option>৪৫৬৯</option>
												</select>
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>জোন</label>
												<input type="text" class="form-control" disabled>
											</div>
											<div class="form-group col-md-3">
												<label>Upazila</label>
												<input type="text" class="form-control" disabled>
											</div>
											<div class="form-group col-md-3">
												<label>Name</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Designation</label>
												<input type="text" class="form-control">
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>টেলিফোন নং</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>ভাউচারের তারিখ</label>
												<input class="form-control date-picker" size="16" type="text">
											</div>
											<div class="form-group col-md-3">
												<label>অর্থবছর</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>গ্রহীতার নাম</label>
												<input type="text" class="form-control">
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>ব্যয়ের মাধ্যম</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>নগদ</option>
													<option>চেক</option>
													<option>টি. টি</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>চেক নং</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Cheque Date</label>
												<input class="form-control date-picker" size="16" type="text">
											</div>
											<div class="form-group col-md-3">
												<label>টাকার পরিমাণ</label>
												<input type="text" class="form-control">
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>ইলেকট্রিক মিটার নং</label>
												<input type="text" class="form-control" readonly>
											</div>
											<div class="form-group col-md-3">
												<label for="exampleInputFile1">ভাউচার সংযুক্ত করুন</label>
												<input type="file" id="exampleInputFile1">
											</div>
											<div class="form-group col-md-6">
												<label>বর্ণনা</label>
												<textarea class="form-control" rows="3"></textarea>
											</div>
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="expense.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>
	</body>
</html>
