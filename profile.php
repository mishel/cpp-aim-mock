<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>Profile</span>
									<i class="fa fa-circle"></i>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Change Profile Info
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												Change Password
											</span>
										</div>
									</div>
									<div class="portlet-body form">
										<div class="row">
											<div class="form-group col-md-3">
												<label>Old Password</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>New Password</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Retype New Password</label>
												<input type="text" class="form-control">
											</div>
										</div>
									</div>
								</div>

								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												Change Info
											</span>
										</div>
									</div>
									<div class="portlet-body form">
										<div class="row">
											<div class="form-group col-md-3">
												<label for="exampleInputFile1">Change/Upload Image</label>
												<input type="file" id="exampleInputFile1">
											</div>
										</div>
									</div>
								</div>

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="head.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>

		<script>
			(function () {
				document.querySelector("#menu_profile").classList.add("active");
			})();
		</script>
	</body>
</html>
