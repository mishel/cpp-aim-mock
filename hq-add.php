<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>Admin</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>HQ</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Edit HQ
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												HQ Info
											</span>
										</div>
									</div>
									<div class="portlet-body form">
										<form action="" class="row">
											<div class="form-group col-md-3">
												<label>Email</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Phone No.</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label>Address</label>
												<textarea class="form-control" rows="3"></textarea>
											</div>
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												HQ Responsible Person
											</span>
										</div>
									</div>
									<div class="portlet-body form">
										<div class="row">
											<div class="form-group col-md-3">
												<label>Select Person</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>General User 1</option>
													<option>General User 2</option>
													<option>General User 3</option>
												</select>
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>Username</label>
												<input type="text" class="form-control" disabled value="User Name">
											</div>
											<div class="form-group col-md-6">
												<label>Full Name</label>
												<input type="text" class="form-control" disabled value="Full Name">
											</div>
											<div class="form-group col-md-3">
												<label>Email</label>
												<input type="text" class="form-control" disabled value="test@test.com">
											</div>
											<div class="form-group col-md-3">
												<label>মোবাইল নং</label>
												<input type="text" class="form-control" disabled value="0171XXXXXXX">
											</div>
											<div class="form-group col-md-3">
												<label>টেলিফোন নং</label>
												<input type="text" class="form-control" disabled value="+8802XXXXXX">
											</div>
											<div class="form-group col-md-3">
												<label for="exampleInputFile1">Image</label>
												<input type="file" id="exampleInputFile1">
											</div>
										</div>
									</div>
								</div>

								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												HQ Accounts Person
											</span>
										</div>
									</div>
									<div class="portlet-body form">
										<div class="row">
											<div class="form-group col-md-3">
												<label>Select Person</label>
												<select id="hq_acct" class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>General User 1</option>
													<option>General User 2</option>
													<option>General User 3</option>
												</select>
											</div>
											<div class="form-group col-md-9">
												<label>Same As</label>
												<div class="mt-radio-inline">
													<label class="mt-radio">
														<input type="radio" name="sameUser1" onclick="disableHQAcct();"> Same as HQ responsible person
														<span></span>
													</label>
													<label class="mt-radio">
														<input type="radio" name="sameUser1" onclick="enableHQAcct();" checked> Select different person
														<span></span>
													</label>
													<label class="mt-radio">
														<input type="radio" name="sameUser1" onclick="disableHQAcct();"> Disable
														<span></span>
													</label>
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>Username</label>
												<input type="text" class="form-control" disabled value="User Name">
											</div>
											<div class="form-group col-md-6">
												<label>Full Name</label>
												<input type="text" class="form-control" disabled value="Full Name">
											</div>
											<div class="form-group col-md-3">
												<label>Email</label>
												<input type="text" class="form-control" disabled value="test@test.com">
											</div>
											<div class="form-group col-md-3">
												<label>মোবাইল নং</label>
												<input type="text" class="form-control" disabled value="0171XXXXXXX">
											</div>
											<div class="form-group col-md-3">
												<label>টেলিফোন নং</label>
												<input type="text" class="form-control" disabled value="+8802XXXXXX">
											</div>
											<div class="form-group col-md-3">
												<label for="exampleInputFile1">Image</label>
												<input type="file" id="exampleInputFile1">
											</div>
										</div>
									</div>
								</div>

								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												HQ Inventory Person
											</span>
										</div>
									</div>
									<div class="portlet-body form">
										<div class="row">
											<div class="form-group col-md-3">
												<label>Select Person</label>
												<select id="hq_inv" class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>General User 1</option>
													<option>General User 2</option>
													<option>General User 3</option>
												</select>
											</div>
											<div class="form-group col-md-6">
												<label>Same As</label>
												<div class="mt-radio-inline">
													<label class="mt-radio">
														<input type="radio" name="sameUser2" onclick="disableHQInv();"> Same as HQ responsible person
														<span></span>
													</label>
													<label class="mt-radio">
														<input type="radio" name="sameUser2" onclick="enableHQInv();" checked> Select different person
														<span></span>
													</label>
													<label class="mt-radio">
														<input type="radio" name="sameUser2" onclick="disableHQInv();"> Disable
														<span></span>
													</label>
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>Username</label>
												<input type="text" class="form-control" disabled value="User Name">
											</div>
											<div class="form-group col-md-6">
												<label>Full Name</label>
												<input type="text" class="form-control" disabled value="Full Name">
											</div>
											<div class="form-group col-md-3">
												<label>Email</label>
												<input type="text" class="form-control" disabled value="test@test.com">
											</div>
											<div class="form-group col-md-3">
												<label>মোবাইল নং</label>
												<input type="text" class="form-control" disabled value="0171XXXXXXX">
											</div>
											<div class="form-group col-md-3">
												<label>টেলিফোন নং</label>
												<input type="text" class="form-control" disabled value="+8802XXXXXX">
											</div>
											<div class="form-group col-md-3">
												<label for="exampleInputFile1">Image</label>
												<input type="file" id="exampleInputFile1">
											</div>
										</div>
									</div>
								</div>

								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												Bank Info
											</span>
										</div>
									</div>
									<div class="portlet-body form" id="bank_info">
										<div class="row">
											<div class="form-group col-md-3">
												<label>Name of the Bank</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Name of the Branch</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-2">
												<label>ব্যাংক হিসাবের নাম</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-2">
												<label>ব্যাংক হিসাবের নং</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-2">
												<label>&nbsp;</label>
												<button class="btn btn-secondary btn-block">Primary Account</button>
											</div>
										</div>
									</div>
									<button class="btn btn-primary" id="add_bank">Add Bank Account</button>
								</div>

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="hq.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>
		<script>
			(function() {
				document.querySelector("#add_bank").addEventListener("click", function() {
					var row = '<div class="row"><div class="form-group col-md-3"><label>Name of the Bank</label><input type="text" class="form-control"></div><div class="form-group col-md-3"><label>Name of the Branch</label><input type="text" class="form-control"></div><div class="form-group col-md-2"><label>ব্যাংক হিসাবের নাম</label><input type="text" class="form-control"></div><div class="form-group col-md-2"><label>ব্যাংক হিসাবের নং</label><input type="text" class="form-control"></div><div class="form-group col-md-2"><label>Select Account Type</label><select class="form-control"><option disabled selected>নির্বাচন করুন</option><option>Red Crescent</option><option>Project 1</option><option>Project 2</option><option>Project 3</option></select></div></div>';
					document.querySelector("#bank_info").insertAdjacentHTML("beforeend", row);
				});
			})();
			function disableHQAcct() {
				document.getElementById("hq_acct").disabled = true;
			}
			function enableHQAcct() {
				document.getElementById("hq_acct").disabled = false;
			}
			function disableHQInv() {
				document.getElementById("hq_inv").disabled = true;
			}
			function enableHQInv() {
				document.getElementById("hq_inv").disabled = false;
			}
		</script>

		<script>
			(function () {
				document.querySelector("#menu_admin").classList.add("active");
				document.querySelectorAll("#menu_admin ul li")[1].classList.add("active");
			})();
		</script>
	</body>
</html>
