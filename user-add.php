<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>Admin</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Add</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Add Admin / Director Admin
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-body form">
										<form action="" class="row">
											<div class="form-group col-md-3">
												<label>Username</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Password</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label>Full Name</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>User Type</label>
												<select class="form-control">
													<option>Admin</option>
													<option>Director Admin</option>
													<option>General User</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>Email</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>মোবাইল নং</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Telephone</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label for="exampleInputFile1">Image Upload</label>
												<input type="file" id="exampleInputFile1">
											</div>
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="user.php" class="btn btn-primary py-2">SAVE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>

		<script>
			(function () {
				document.querySelector("#menu_admin").classList.add("active");
				document.querySelectorAll("#menu_admin ul li")[0].classList.add("active");
			})();
		</script>
	</body>
</html>
