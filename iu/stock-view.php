<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
						<ul class="page-breadcrumb">
								<li>
									<span>Inventory</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Stock</span>
								</li>
								<li>
									<span>Electronics</span>
								</li>
								<li>
									<span>Computer</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Computer Details
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-title text-right">

									</div>
									<div class="portlet-body row">
										<div class="col-md-2"><Strong>Category</Strong></div>
										<div class="col-md-2">Electronics</div>
										<div class="col-md-2"><Strong>Product Name</Strong></div>
										<div class="col-md-2">Computer</div>
									</div>
								</div>
								<div class="portlet light bordered">
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-hover order-column" id="sample_1">
											<thead>
												<tr>
													<th>Zone/Upazilla</th>
													<th>Quantity</th>
													<th>Issue Date</th>
												</tr>
											</thead>
											<tbody>
												<tr class="odd gradeX">
													<td>Khulna</td>
													<td>5</td>
													<td>27/01/2019</td>
												</tr>
												<tr class="odd gradeX">
													<td>Rajshahi</td>
													<td>7</td>
													<td>27/01/2019</td>
												</tr>
												<tr class="odd gradeX">
													<td>Ctg</td>
													<td>11</td>
													<td>27/01/2019</td>
												</tr>
											</tbody>
										</table>
										<ul>
											<li>How many in stores....</li>
										</ul>
									</div>
								</div>
								<!-- END PORTLET-->
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>
	</body>
</html>
