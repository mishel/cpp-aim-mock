<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>Inventory</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Loss/Destroy/Expired</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Add Loss/Destroy/Expired
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<!-- <div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												Portlet Captions
											</span>
										</div>
									</div> -->
									<div class="portlet-body form">
										<form action="" class="row">
											<div class="form-group col-md-3">
												<label>জোন</label>
												<select class="form-control">
													<option>জোন ১</option>
													<option>জোন ২</option>
													<option>জোন ৩</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>স্টেশন</label>
												<select class="form-control">
													<option>স্টেশন ১</option>
													<option>স্টেশন ২</option>
													<option>স্টেশন ৩</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>Category</label>
												<select class="form-control">
													<option>Category 1</option>
													<option>Category 2</option>
													<option>Category 3</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>Product</label>
												<select class="form-control">
													<option>Product 1</option>
													<option>Product 2</option>
													<option>Product 3</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>Person</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>তারিখ</label>
												<input class="form-control date-picker" size="16" type="text">
											</div>
											<div class="form-group col-md-6">
												<label>মন্তব্য</label>
												<textarea class="form-control" rows="3"></textarea>
											</div>


											<!-- <div class="col-md-12">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>Category</th>
															<th>Products</th>
															<th>Quantity</th>
															<th>Unit Price</th>
															<th>Total</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>
																<div class="form-group">
																	<select class="form-control">
																		<option disabled selected>নির্বাচন করুন</option>
																		<option>Category 1</option>
																		<option>Category 2</option>
																		<option>Category 3</option>
																	</select>
																</div>
															</td>
															<td>
																<div class="form-group">
																	<select class="form-control">
																		<option disabled selected>নির্বাচন করুন</option>
																		<option>Product 1</option>
																		<option>Product 2</option>
																		<option>Product 3</option>
																	</select>
																</div>
															</td>
															<td>
																<div class="form-group">
																	<input type="text" class="form-control">
																</div>
															</td>
															<td>
																<div class="form-group">
																	<input type="text" class="form-control">
																</div>
															</td>
															<td>
																<div class="form-group">
																	<input type="text" class="form-control">
																</div>
															</td>
															<td>
																<button type="button" class="btn btn-primary btn-sm" id="addRow">Add</button>
															</td>
														</tr>
													</tbody>
												</table>
											</div> -->
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="receive.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
									<ul>

									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>
		<script>
			(function() {
				document.querySelector("#addRow").addEventListener("click", function(e) {
					var td = e.target.parentNode;
					var tr = td.parentNode;
					var tbody = tr.parentNode;
					var tds = tr.querySelectorAll("td");
					var category = tds[0].querySelector("select").value;
					var product = tds[1].querySelector("select").value;
					var q = tds[2].querySelector("input").value;
					var up = tds[3].querySelector("input").value;
					var t = tds[4].querySelector("input").value;
					tds[0].querySelector("select").selectedIndex = 0;
					tds[1].querySelector("select").selectedIndex = 0;
					tds[2].querySelector("input").value = "";
					tds[3].querySelector("input").value = "";
					tds[4].querySelector("input").value = "";
					var row = `<tr><td>${category}</td><td>${product}</td><td>${q}</td><td>${up}</td><td>${t}</td><td><button class="btn btn-danger btn-sm">Delete</button></td></tr>`;
					tbody.insertAdjacentHTML("afterbegin", row);


				})
			})();
		</script>
	</body>
</html>
