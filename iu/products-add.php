<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>Inventory</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Products</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Add Product
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<span class="caption-subject font-dark bold uppercase">
												Portlet Captions
											</span>
										</div>
									</div>
									<div class="portlet-body form">
										<form action="" class="row">
											<div class="form-group col-md-3">
												<label>Product Name</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>Product Code</label>
												<input type="text" class="form-control" placeholder="auto generate" readonly>
											</div>
											<div class="form-group col-md-3">
												<label>Category</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>Category 1</option>
													<option>Category 2</option>
													<option>Category 3</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>Unit</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>Pc</option>
													<option>Dozen</option>
													<option>Kg</option>
												</select>
											</div>
											<div class="form-group col-md-6">
												<label>বর্ণনা</label>
												<textarea class="form-control" rows="3"></textarea>
											</div>
											<div class="form-group col-md-3">
												<label>Opening Stock</label>
												<input type="text" class="form-control">
											</div>
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="products.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
									<ul>
										<li>Product code - auto generate</li>
										<li>Add option for perishable/non parishable</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>
	</body>
</html>
