<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>হিসাবরক্ষণ</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>অর্থ ফেরত</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							অর্থ ফেরত
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-body form">
										<form action="" class="row">
											<!-- <div class="form-group col-md-3">
												<label>Source</label>
												<select class="form-control" id="src">
													<option disabled selected>নির্বাচন করুন</option>
													<option>স্টেশন</option>
													<option>ব্যক্তি</option>
												</select>
											</div> -->
											<div class="form-group col-md-3">
												<label>জোন</label>
												<input type="text" class="form-control" value="Khulna" disabled>
											</div>
											<div class="form-group col-md-3">
												<label>স্টেশন</label>
												<input type="text" class="form-control" value="Khulna" disabled>
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>ব্যাংকের নাম</label>
												<input type="text" class="form-control" readonly>
											</div>
											<div class="form-group col-md-3">
												<label>শাখার নাম</label>
												<input type="text" class="form-control" readonly>
											</div>
											<div class="form-group col-md-3">
												<label>ব্যাংক হিসাবের নাম</label>
												<input type="text" class="form-control" readonly>
											</div>
											<div class="form-group col-md-3">
												<label>ব্যাংক হিসাব নম্বর</label>
												<input type="text" class="form-control" readonly>
											</div>

											<div class="form-group col-md-3">
												<label>অর্থ ফেরতের পদ্ধতি</label>
												<select class="form-control">
													<option disabled selected>নির্বাচন করুন</option>
													<option>চেক</option>
													<option>ব্যাংক ড্রাফট</option>
													<option>টি. টি</option>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>রেফারেন্স নং</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>টাকার পরিমাণ</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label>টাকার পরিমাণ (কথায়)</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>তারিখ</label>
												<input class="form-control date-picker" size="16" type="text">
											</div>
											<div class="form-group col-md-3">
												<label for="exampleInputFile1">ব্যাংক স্লিপ সংযুক্ত করুন</label>
												<input type="file" id="exampleInputFile1">
											</div>
											<div class="form-group col-md-6">
												<label>মন্তব্য</label>
												<textarea class="form-control" rows="3"></textarea>
											</div>

											<div class="clearfix"></div>

											<div class="col-md-12">
												<div class="table-responsive">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th></th>
																<th>কোড-হেড</th>
																<th>কোড-সাবহেড</th>
																<th>পরিমাণ</th>
																<th>মন্তব্য</th>
															</tr>
														</thead>
														<tbody id="tbody">
															<tr>
																<td>১</td>
																<td>
																	<select class="form-control">
																		<option selected>১২৩৪</option>
																		<option>১২৩৫</option>
																		<option>১২৩৬</option>
																	</select>
																</td>
																<td>
																	<select class="form-control">
																		<option selected>৪৫৬৭</option>
																		<option>৪৫৬৮</option>
																		<option>৪৫৬৯</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" value="১,০০,০০০">
																</td>
																<td>
																	<input type="text" class="form-control" >
																</td>
															</tr>
															<tr>
																<td>২</td>
																<td>
																	<select class="form-control">
																		<option selected>১২৩৪</option>
																		<option>১২৩৫</option>
																		<option>১২৩৬</option>
																	</select>
																</td>
																<td>
																	<select class="form-control">
																		<option>৪৫৬৭</option>
																		<option selected>৪৫৬৮</option>
																		<option>৪৫৬৯</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" value="১,৫০,০০০">
																</td>
																<td>
																	<input type="text" class="form-control" >
																</td>
															</tr>
															<tr>
																<td>৩</td>
																<td>
																	<select class="form-control">
																		<option>১২৩৪</option>
																		<option selected>১২৩৫</option>
																		<option>১২৩৬</option>
																	</select>
																</td>
																<td>
																	<select class="form-control">
																		<option selected>৪৫৬৭</option>
																		<option>৪৫৬৮</option>
																		<option>৪৫৬৯</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" value="২,০০,০০০">
																</td>
																<td>
																	<input type="text" class="form-control" >
																</td>
															</tr>
															<tr>
																<td>৪</td>
																<td>
																	<select class="form-control">
																		<option>১২৩৪</option>
																		<option selected>১২৩৫</option>
																		<option>১২৩৬</option>
																	</select>
																</td>
																<td>
																	<select class="form-control">
																		<option>৪৫৬৭</option>
																		<option selected>৪৫৬৮</option>
																		<option>৪৫৬৯</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" value="২,৫০,০০০">
																</td>
																<td>
																	<input type="text" class="form-control" >
																</td>
															</tr>
															<tr>
																<td>৫</td>
																<td>
																	<select class="form-control">
																		<option>১২৩৪</option>
																		<option>১২৩৫</option>
																		<option selected>১২৩৬</option>
																	</select>
																</td>
																<td>
																	<select class="form-control">
																		<option selected>৪৫৬৭</option>
																		<option>৪৫৬৮</option>
																		<option>৪৫৬৯</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" value="৩,০০,০০০">
																</td>
																<td>
																	<input type="text" class="form-control" >
																</td>
															</tr>
															<tr>
																<td>৬</td>
																<td>
																	<select class="form-control">
																		<option>১২৩৪</option>
																		<option>১২৩৫</option>
																		<option selected>১২৩৬</option>
																	</select>
																</td>
																<td>
																	<select class="form-control">
																		<option>৪৫৬৭</option>
																		<option>৪৫৬৮</option>
																		<option selected>৪৫৬৯</option>
																	</select>
																</td>
																<td>
																	<input type="text" class="form-control" value="৪,০০,০০০">
																</td>
																<td>
																	<input type="text" class="form-control" >
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<!-- <button type="button" id="add_row" class="btn btn-primary">আরও যুক্ত করুন</button> -->
											</div>
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="gov_cash_return.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>

		<script>
			(function () {
				document.querySelector("#gov_acct").classList.add("active");
				document.querySelectorAll("#gov_acct ul li")[2].classList.add("active");
			})();
		</script>

		<script>
			// (function() {
			// 	document.querySelector("#src").addEventListener("change", function(e) {
			// 		var val = e.target.value;
			// 		var blocks = document.querySelectorAll(".src");
			// 		if (val === "Station") {
			// 			blocks.forEach((el) => {
			// 				el.classList.add("hidden");
			// 			});
			// 			document.querySelector("#station").classList.remove("hidden");
			// 		}
			// 		else if (val === "Person") {
			// 			blocks.forEach((el) => {
			// 				el.classList.add("hidden");
			// 			});
			// 			document.querySelector("#person").classList.remove("hidden");
			// 		}
			// 	});
			// })();
		</script>
	</body>
</html>
