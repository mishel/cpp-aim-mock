<!DOCTYPE html>
<html lang="en">
	<!-- BEGIN HEAD -->
	<?php include './shared/head.html'; ?>
	<!-- END HEAD -->

	<body
		class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	>
		<div class="page-wrapper">
			<!-- BEGIN Topbar -->
			<?php include './shared/topbar.html'; ?>
			<!-- END Topbar -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"></div>
			<!-- END HEADER & CONTENT DIVIDER -->
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include './shared/sidebar.html'; ?>
				<!-- END SIDEBAR -->
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								<li>
									<span>হিসাবরক্ষণ</span>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Cash Receive</span>
								</li>
							</ul>
						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
							Cash Receive
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<div class="row">
							<div class="col-lg-12">
								<!-- BEGIN PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-body form">
										<form action="" class="row">
											<div class="form-group col-md-3">
												<label>Source</label>
												<select class="form-control" id="src" disabled>
													<option disabled selected>নির্বাচন করুন</option>
													<option selected>Govt.</option>
													<option>Others</option>
												</select>
											</div>
											<div id="govt" class="src hiddenx">
												<div class="form-group col-md-3">
													<label>কোড-হেড</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>১২৩৪</option>
														<option>১২৩৫</option>
														<option>১২৩৬</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>কোড-সাবহেড</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>৪৫৬৭</option>
														<option>৪৫৬৮</option>
														<option>৪৫৬৯</option>
													</select>
												</div>
											</div>
											<div id="others" class="src hidden">
												<div class="form-group col-md-3">
													<label>Others Head</label>
													<select class="form-control">
														<option disabled selected>নির্বাচন করুন</option>
														<option>Head1</option>
														<option>Head2</option>
														<option>Head3</option>
													</select>
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="form-group col-md-3">
												<label>টাকার পরিমাণ</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label>টাকার পরিমাণ (কথায়)</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group col-md-3">
												<label>তারিখ</label>
												<input class="form-control date-picker" size="16" type="text">
											</div>
											<div class="form-group col-md-6">
												<label>মন্তব্য</label>
												<textarea class="form-control" rows="3"></textarea>
											</div>
										</form>
									</div>
								</div>
								<!-- END PORTLET-->

								<div class="portlet light bordered">
									<div class="portlet-body text-center">
										<a href="gov_cash_receive.php" class="btn btn-primary">সংরক্ষণ করুন</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include './shared/footer.html'; ?>
			<!-- END FOOTER -->
		</div>

		<!-- All Scripts -->
		<?php include './shared/scripts.html'; ?>

		<script>
			(function () {
				document.querySelector("#gov_acct").classList.add("active");
				document.querySelectorAll("#gov_acct ul li")[1].classList.add("active");
			})();
		</script>

		<script>
			(function() {
				document.querySelector("#src").addEventListener("change", function(e) {
					var val = e.target.value;
					var blocks = document.querySelectorAll(".src");
					if (val === "Govt.") {
						blocks.forEach((el) => {
							el.classList.add("hidden");
						});
						document.querySelector("#govt").classList.remove("hidden");
					}
					else if (val === "Others") {
						blocks.forEach((el) => {
							el.classList.add("hidden");
						});
						document.querySelector("#others").classList.remove("hidden");
					}
				});
			})();
		</script>
	</body>
</html>
